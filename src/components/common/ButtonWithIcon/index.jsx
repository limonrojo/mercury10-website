import React from 'react';
import './style.scss';

const ButtonWithIcon = ({buttonData, onClickHandler}) => {
    return (
        <div className="ButtonWithIcon" onClick={()=>onClickHandler()}>
            <div className="body">
                {buttonData.buttonText}
            </div>
            <div className="icon">
                {buttonData.buttonIcon}
            </div>
        </div>
    )
}

export default ButtonWithIcon