import React from 'react';
import './style.scss'

const HexagonBackground = () => {
    return (
        <div className="HexagonBackground">
            <div className="hexagon-base-glow hexagon-base-glow-1"></div>
            <div className="hexagon-base-glow hexagon-base-glow-2"></div>
            <div className="hexagon-base"></div>
        </div>
    )
}

export default HexagonBackground
