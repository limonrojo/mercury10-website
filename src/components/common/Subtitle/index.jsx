import React from 'react';
import './style.scss';

const Subtitle = ({children}) => {
    return (
        <div className="Subtitle">
            {children}
        </div>
    )
}

export default Subtitle
