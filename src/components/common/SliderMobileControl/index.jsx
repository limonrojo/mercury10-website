import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronLeft,faChevronRight  } from '@fortawesome/free-solid-svg-icons';
import './style.scss';

const SliderMobileControl = ({previousButtonClickHandler, nextButtonClickHandler}) => {
    return (
        <div className="SliderMobileControl">
            <button 
                className="previous"
                
                onClick={()=>{previousButtonClickHandler("Prev")}}
            >
                <FontAwesomeIcon icon={faChevronLeft} />
            </button>
            <button 
                className="next"
                onClick={()=>{nextButtonClickHandler("Next")}}
            >
                <FontAwesomeIcon icon={faChevronRight} />
            </button>
        </div>
    )
}

export default SliderMobileControl
