import React from 'react';
import './style.scss';

const Title = ({children, align}) => {
    let alignClass;
    (align === "right" || align === "center") ? alignClass = align : alignClass = '';
    return (
        <div className={`Title ${alignClass}`}>
            {children}
        </div>
    )
}

export default Title
