import React from 'react'
import Control from './Control';
import SliderCounter from 'components/common/SliderCounter';

const Front = ({controlAction, totalSlides, activeSlide, header, showCounter}) => {
    
    return (
        <div className="Front">
            {header}
            <Control controlAction={(action)=>controlAction(action)} />
            {
                (showCounter === true)
                ? <SliderCounter activeSlide={activeSlide} totalSlides={totalSlides} />
                : ''
            }
        </div>
    )
}

export default Front
