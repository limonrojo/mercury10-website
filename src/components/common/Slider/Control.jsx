import SliderButtons from 'components/common/SliderButtons'
import React from 'react'


const Control = ({controlAction}) => {
    return (
        <div className="Control">
            <SliderButtons
                previousButtonClickHandler={ () => controlAction("previous") }
                nextButtonClickHandler={ () =>controlAction("next") }
            />
        </div>
    )
}

export default Control
