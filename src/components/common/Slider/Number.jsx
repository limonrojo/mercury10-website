import React from 'react'

const Number = ({children}) => {
    return (
        <div className="Number">
            {children}
        </div>
    )
}

export default Number
