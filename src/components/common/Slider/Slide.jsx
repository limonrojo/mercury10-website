import React from 'react'
import PropTypes from 'prop-types';
import Image from 'components/common/Image'
import Number from './Number'
import Texts from './Texts'

const Slide = ({number, image, back, title, description, className, showNumbers, links}) => {
    return (
        <div className={`Slide ${(className!==undefined)?className:''}`}>
            {
                (showNumbers === true)
                ? <Number>{number}</Number>
                : ''
            }
            <Image src={image} alt={title} />
            <Texts title={title} description={description} links={links} />
            {
                (back !== '')
                ? <Image src={back} alt="{title}" className="back" />
                : ''
            }
        </div>
    )
}
Slide.propTypes = {
    back: PropTypes.string.isRequired
}
Slide.defaultProps = {
    back: ''
}

export default Slide