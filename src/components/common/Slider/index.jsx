import React from 'react';
import PropTypes from 'prop-types';
import 'auxiliar'
import Front from './Front';
import Slides from './Slides';
import useSlider from 'hooks/useSlider';

const Slider = ({slides, frontHeader, showCounter, showNumbers}) => {
    const [activeSlide, controlAction] = useSlider(slides)
    return (
        <div className="Slider">
            <Slides slides={slides} activeSlide={activeSlide} showNumbers={showNumbers} controlAction={controlAction} />
            <Front
                controlAction={controlAction}
                totalSlides={slides.length}
                activeSlide={activeSlide}
                header={frontHeader}
                showCounter={showCounter}
            />
        </div>
    )
}


Slider.propTypes = {
    showCounter: PropTypes.bool,
    showNumbers: PropTypes.bool,
}
Slider.defaultProps = {
    showCounter: true,
    showNumbers: true,
}

export default Slider
