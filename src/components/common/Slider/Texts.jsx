import React from 'react'

const Texts = ({title, description, links}) => {
    const renderLink = (link, index) => {
        return(
            <li key={index}><a href={link.url}>{link.text}</a></li>
        )
    }
    const renderLinks = (links) => {
        if(links !== undefined){
            if(links.length > 0){
                return(
                    <div className="links">
                        <ul>
                            {links.map(renderLink)}
                        </ul>
                    </div>
                )
            }
        }

    }
    return (
        <div className="Texts">
            <div className="title">{title}</div>
            <div className="description">{description}</div>
            {renderLinks(links)}
        </div>
    )
}

export default Texts
