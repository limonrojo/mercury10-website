import React from 'react';
import Slide from './Slide';
import SliderMobileControl from '../SliderMobileControl'
import getSlidesInitialClasses from 'utils/getInitialClasses';

const Slides = ({slides, activeSlide, showNumbers, controlAction}) => {
    const renderSlides = () => {
        const initialClasses = getSlidesInitialClasses(activeSlide, slides.length)

        const renderSlide = (slide, index) => (
            <Slide
                key={index.toString()}
                number={index+1}
                image={slide.image}
                back={(slide.back !== undefined)?slide.back !== undefined:''}
                title={slide.title}
                links={slide.links}
                description={slide.description}
                className={initialClasses[index]}
                showNumbers={showNumbers}
            />   
        )
        return slides.map(renderSlide);
    }

    return (
        <div className="Slides">
            {renderSlides()}
            <SliderMobileControl 
                previousButtonClickHandler={ () => controlAction("previous") }
                nextButtonClickHandler={ () =>controlAction("next") }
            />
        </div>
    )
}

export default Slides
