import React from 'react'
import 'auxiliar'
import './style.scss'

const SliderCounter = ({activeSlide, totalSlides}) => {
    return (
        <div className="SliderCounter">
            <div className="activeSlide">{(activeSlide+1).pad(2)}</div>
            <div className="totalSlides">/ {totalSlides.pad(2)}</div>
        </div>
    )
}

export default SliderCounter
