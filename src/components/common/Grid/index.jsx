import React from 'react';
import './style.scss';

const Grid = ({children}) => {
    return (
        <div className="Grid">
            {children}
        </div>
    )
}

export default Grid
