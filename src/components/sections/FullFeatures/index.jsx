import React from 'react';
import { useTranslation } from 'react-i18next';
import Title from 'components/common/Title';
import './style.scss';
import HardwareIcon from './assets/hardware-icon.png';
import SoftwareIcon from './assets/software-icon.png';
import Image from 'components/common/Image';

const FullFeatures = () => {
    const { t } = useTranslation()

    const renderSpecifications = () => {
        const columns = ["Hardware", "Softaware"];
        const listIcons = {
            "Hardware":     HardwareIcon,
            "Softaware":    SoftwareIcon
        }

        const renderSpecificationsList = (list, index) => {

            const listContent = t('FullFeatures.content.'+list);
            const listItems = Object.values(listContent.items);

            const renderSpecificationsListItem = (item, index) => {
                return(
                    <li key={`${list}-${index}`}>{item}</li>
                )
            }

            return(
                <div className="column" key={`${list}-${index}`}>
                    <div className="title">
                        <div className="icon">
                            <Image src={listIcons[list]} alt={list} />
                        </div>
                        <div className="text">{t('FullFeatures.content.'+list+'.title')}</div>
                    </div>
                    <ul>
                        { listItems.map(renderSpecificationsListItem) }
                    </ul>
                </div>
            )
        }

        return columns.map(renderSpecificationsList)
    }
    return (
        <section id="FullFeatures">
            <div className="container">
                <header className="gradientBordersBanner">
                    <Title align="center">Full Features</Title>
                </header>
                <div className="content">
                    <div className="columns">
                        {renderSpecifications()}
                    </div>
                </div>
            </div>
        </section>
    )
}

export default FullFeatures
