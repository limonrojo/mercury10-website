import React from 'react';
import { useTranslation } from 'react-i18next';
import Mailchimp from 'react-mailchimp-form'
import './style.scss';

const Banner2 = () => {
    const { t } = useTranslation()
    return (
        <section id="Banner2" className="Banner">
            <div className="container">
                <div className="columns">
                    <div className="column">
                        <div className="text">{t('Banner2.text-1')}</div>
                        <div className="textSmall">{t('Banner2.text-2')}</div>
                    </div>
                    <div className="column">
                        <Mailchimp
                            className="form"
                            action={'https://mercury10.us1.list-manage.com/subscribe/post?u=9c228e54140491fa6486172b0&amp;id=8ceb81223c'}
                            fields={[
                                {
                                    name: 'EMAIL',
                                    placeholder: 'Email',
                                    type: 'email',
                                    required: true
                                },
                                {
                                    name: 'FNAME',
                                    placeholder: 'Full Name',
                                    type: 'text',
                                    required: false
                                },
                            ]}
                        />
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Banner2
