import React from 'react';
import { useTranslation } from 'react-i18next';
import Subtitle from 'components/common/Subtitle';
import Title from 'components/common/Title';
import './style.scss';
import Slider from 'components/common/Slider';
import AutomotiveImage from './assets/story-automotive.png'
import EngineeringImage from './assets/story-engineering.png'
import DIYImage from './assets/domotic.png'

const SuccessStories = () => {
    const { t } = useTranslation()
    const successStories = [
        {
            image:      AutomotiveImage,
            title:      t('SuccessStories.Slider.Slides.1.title'),
            description:t('SuccessStories.Slider.Slides.1.description'),
            links:      [
                {url: 'https://favarmor.com/', text: "FAV"},
                {url: 'https://ferbel.com/', text: "Ferbel"},
            ]
        },
        {
            image:      EngineeringImage,
            title:      t('SuccessStories.Slider.Slides.2.title'),
            description:t('SuccessStories.Slider.Slides.2.description'),
            links:      [
                {url: 'https://herbazal.com/camp4hens/', text: "Herbazal"},
            ]
        },
        {
            image:      DIYImage,
            title:      t('SuccessStories.Slider.Slides.3.title'),
            description:t('SuccessStories.Slider.Slides.3.description')
        },
        {
            image:      AutomotiveImage,
            title:      t('SuccessStories.Slider.Slides.1.title'),
            description:t('SuccessStories.Slider.Slides.1.description'),
            links:      [
                {url: 'https://favarmor.com/', text: "FAV"},
                {url: 'https://ferbel.com/', text: "Ferbel"},
            ]
        },
        {
            image:      EngineeringImage,
            title:      t('SuccessStories.Slider.Slides.2.title'),
            description:t('SuccessStories.Slider.Slides.2.description'),
            links:      [
                {url: 'https://herbazal.com/camp4hens/', text: "Herbazal"},
            ]
        },
        {
            image:      DIYImage,
            title:      t('SuccessStories.Slider.Slides.3.title'),
            description:t('SuccessStories.Slider.Slides.3.description')
        },
    ];
    return (
        <section id="SuccessStories">
            <div className="container">
                <header>
                    <Title align="center">{ t('SuccessStories.header.Title') }</Title>
                    <Subtitle>{ t('SuccessStories.header.Subtitle') }</Subtitle>
                </header>
            </div>
            <Slider slides={successStories} showCounter={false} showNumbers={false} />
        </section>
    )
}

export default SuccessStories
