import React from 'react'

const Use = ({icon, title, description}) => {

    return (
        <div className="Use hoverZoom">
            <div className="icon"><img src={icon} alt={title} /></div>
            <div className="title">{title}</div>
            <div className="description">{description}</div>
        </div>
    )
}

export default Use
