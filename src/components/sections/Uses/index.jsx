import React from 'react';
import { useTranslation } from 'react-i18next'
import Title from 'components/common/Title';
import Subtitle from 'components/common/Subtitle';
import Grid from 'components/common/Grid';
import Image from 'components/common/Image'
import Use from './Use';
import Shields from './assets/shields.png';
import Monitor from './assets/monitor.png';
import Multimeter from './assets/multimeter.png';
import Oscilloscope from './assets/oscilloscope.png';
import VoiceControl from './assets/voice-control.png';
import Cloud from './assets/cloud.png';
import OpenSoftware from './assets/open-source.png';
import OpenHardware from './assets/open-source-hardware.png';
import './style.scss';

const Uses = () => {
    const { t } = useTranslation()
    const uses = [
        {
            icon:   Shields,
            title:  t('Uses.Grid.1.title'),
            description:   t('Uses.Grid.1.description')
        },
        {
            icon:   Monitor,
            title:  t('Uses.Grid.2.title'),
            description:   t('Uses.Grid.2.description')
        },
        {
            icon:   Multimeter,
            title:  t('Uses.Grid.3.title'),
            description:   t('Uses.Grid.3.description')
        },
        {
            icon:   Oscilloscope,
            title:  t('Uses.Grid.4.title'),
            description:   t('Uses.Grid.4.description')
        },
        {
            icon:   VoiceControl,
            title:  t('Uses.Grid.5.title'),
            description:   t('Uses.Grid.5.description')
        },
        {
            icon:   Cloud,
            title:  t('Uses.Grid.6.title'),
            description:   t('Uses.Grid.6.description')
        },
    ];

    const renderUses = () => {
        const renderUse = (use, index) => (
            <Use
                key={index.toString()}
                icon={use.icon}
                title={use.title}
                description={use.description}
            />   
        )
        return uses.map(renderUse);
    }

    return (
        <section id="Uses">
            <div className="container">
                <div className="columns">
                    <div className="column">
                        <header>
                            <Title>{t('Uses.header.Title')}</Title>
                            <Subtitle>
                                {t('Uses.header.Subtitle.1')}<br/>
                                {t('Uses.header.Subtitle.2')}
                            </Subtitle>
                        </header>
                        <div className="text">
                            <p>{t('Uses.text-1')}</p>
                            <p>{t('Uses.text-2')}</p>
                            
                            <div className="images">
                                <ul>
                                    <li><Image src={OpenSoftware} /></li>
                                    <li><Image src={OpenHardware} /></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="column">
                        <Grid>
                            { renderUses() }    
                        </Grid>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Uses
