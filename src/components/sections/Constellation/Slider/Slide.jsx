import React from 'react'
import { connect } from 'react-redux'
import HexagonBackground from 'components/common/HexagonBackground'
import Image from 'components/common/Image'

const Slide = ({data, className, loadPopUp}) => {
    const buttonClickHandler = () => {
        loadPopUp(data.button.actionData)
    }
    return (
        <div className={`Slide ${className}`}>
            <div className='image'>
                <HexagonBackground />
                <Image src={data.image} />
            </div>
            <div className='data'>
                <div className='title'>{data.title}</div>
                <div className='description'>{data.description.map((paragraph, index) =><p>{paragraph}</p>)}</div>
                {
                    data.button !== undefined &&
                    <div className='button'><button onClick={buttonClickHandler}>{data.button.text}</button></div>
                }
            </div>
        </div>
    )
}
const mapDispatchToProps = dispatch => {
    return({
        loadPopUp: (buttonActionData) => {
            dispatch({
                type: 'LOAD_POPUP',
                payload: {
                    data: buttonActionData
                }
            })
        }
    });
}
export default connect(undefined, mapDispatchToProps)(Slide)
