import React from 'react'
import getSlidesInitialClasses from 'utils/getInitialClasses'
import Slide from './Slide'

const Slides = ({slidesData, activeSlide, controlAction}) => {
    const initialClasses = getSlidesInitialClasses(activeSlide, slidesData.length)
    return (
        <div className="Slides">
            {slidesData.map((slideData, index) => (
                <Slide
                    key={index}
                    data={slideData}
                    className={initialClasses[index]}
                /> 
            ))}
        </div>
    )
}

export default Slides
