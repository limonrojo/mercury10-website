import Image from 'components/common/Image'
import React from 'react'
import arrow from './../assets/arrow.png'

const Controls = ({controlAction}) => {
    const leftClickHandler = () => controlAction('previous')
    const rightClickHandler = () => controlAction('next')
    return (
        <div className="Controls">
            <button className="left" onClick={leftClickHandler}><Image src={arrow} alt='Left'/></button>
            <button className="right" onClick={rightClickHandler}><Image src={arrow} alt='Right'/></button>
        </div>
    )
}

export default Controls
