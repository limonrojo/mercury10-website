import React from 'react'
import { useTranslation } from 'react-i18next'
import slide1 from './../assets/slide-1.png'
import slide2 from './../assets/slide-2.png'
import slide3 from './../assets/slide-3.png'
import slide4 from './../assets/slide-4.png'
import Slides from './Slides'
import Controls from './Controls'
import useSlider from 'hooks/useSlider'

const Slider = () => {
    const { t } = useTranslation()
    const tBase = 'Constellation.Slider.Slides.'
    const slidesData = [
        {
            image:      slide1,
            title:      t(`${tBase}1.title`),
            description:t(`${tBase}1.description`),
            /*
            button:         {
                text:     "MORE INFO",
                actionData: () => (
                    <React.Fragment>
                        <h1>GPS/LoRa shield</h1>
                        <ul>
                            <li>12 to 5 V. power supply for Mercury10 Board.</li>
                            <li>NEO-6M U-Box GPS module.</li>
                            <li>50 ohm external GPS antenna connector.</li>
                            <li>Over distance Serial Port, up to 200 meters at 9600 baud rate.</li>
                            <li>LoRa connecting Port.</li>
                            <li>JoyStick connecting Port.</li>
                            <li>Built-in indicator LEDs.</li>
                            <li>External indication LED connection port.</li>
                        </ul>
                    </React.Fragment>
                )
            },
            */
        },
        {
            image:      slide2,
            title:      t(`${tBase}2.title`),
            description:t(`${tBase}2.description`),
            
        },
        {
            image:      slide3,
            title:      t(`${tBase}3.title`),
            description:t(`${tBase}3.description`),
        },
        {
            image:      slide4,
            title:      t(`${tBase}4.title`),
            description:t(`${tBase}4.description`),
        }
    ]
    const [activeSlide, controlAction] = useSlider(slidesData, undefined, false)
    return (
        <div className="Slider">
            <div className="main">
                <Slides slidesData={slidesData} activeSlide={activeSlide} />
            </div>
            <Controls controlAction={controlAction}  />
        </div>
    )
}

export default Slider
