import React from 'react'
import Slider from './Slider'
import Subtitle from 'components/common/Subtitle'
import Title from 'components/common/Title'
import { useTranslation } from 'react-i18next'
import './style.scss'

const Constellation = () => {
    const { t } = useTranslation()
    const tBase = 'Constellation.'

    return (
        <section id="Constellation">
            <div className="container">
                <header>
                    <Title align="center">{ t(`${tBase}header.Title`) }</Title>
                    <Subtitle>{ t(`${tBase}header.Subtitle`) }</Subtitle>
                </header>
            </div>
            <Slider />
        </section>
    )
}

export default Constellation