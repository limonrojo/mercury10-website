import React, {useEffect} from 'react'
import { useTranslation } from 'react-i18next'
import Isologo from 'images/isologo.png'

const Table = () => {
    const { t } = useTranslation()
    const comparisonTableRows = [1,2,3,4,5,6,7,8,9];

    useEffect(() => {
        const resizeListener = () => {
            let Table = document.getElementsByClassName("Table")[0];
            let TableContent = Table.children[0];
            let TableWidth = Table.offsetWidth
            let TableContentWidth = TableContent.offsetWidth
            document.getElementsByClassName("Table")[0].scrollLeft = (TableContentWidth - TableWidth) / 2;
        };
        window.addEventListener('resize', resizeListener);
        resizeListener();
    });

    const renderRows = () => {
        const renderRow = (row, index) => (
            <div className="row" key={`comparisonTableRow-${index}`}>
                <div className="column microcontroller">
                    <span className="cellHeader">Microcontroller</span>
                    { t(`Comparison.Table.microcontroller.${row}`) }</div>
                <div className="column mercury10">
                    <span className="cellHeader">Mercury10</span>
                    { t(`Comparison.Table.mercury10.${row}`) }
                </div>
                <div className="column microcomputer">
                    <span className="cellHeader">Microcomputer</span>
                    { t(`Comparison.Table.microcomputer.${row}`) }
                </div>
            </div>
        )
        return comparisonTableRows.map(renderRow)
    }
    return (
        <div className="Table">
            <div className="content">
                <div className="row header">
                    <div className="column microcontroller">
                        { t('Comparison.Table.microcontroller.header') }
                    </div>
                    <div className="column mercury10"><img src={Isologo} alt="Mercury10" /></div>
                    <div className="column microcomputer">Single-board computer</div>
                </div>
                {renderRows()}
            </div>
        </div>
    )
}

export default Table
