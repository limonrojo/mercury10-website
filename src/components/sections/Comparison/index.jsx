import React from 'react';
import { useTranslation } from 'react-i18next'
import Title from 'components/common/Title';
import Subtitle from 'components/common/Subtitle';
import Table from './Table';
import './style.scss';

const Comparison = () => {
    const { t } = useTranslation()
    const tBase = 'Comparison.'
    return (
        <section id="Comparison">
            <div className="container">
                <header>
                    <Title align="center">{ t(`${tBase}header.Title`) }</Title>
                    <Subtitle>{ t(`${tBase}header.Subtitle`) }</Subtitle>
                </header>
                <Table />
            </div>
        </section>
    )
}

export default Comparison
