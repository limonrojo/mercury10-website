import React from 'react';
import { useTranslation } from 'react-i18next'
import Button from 'components/common/Button';
import './style.scss';

const Banner1 = () => {
    const { t } = useTranslation()
    return (
        <section id="Banner1" className="Banner">
            <div className="container">
                <div className="text">{ t('Banner1.text-1') }</div>
                <Button onClickHandler={() => document.getElementById('Banner2').scrollIntoView()}>{ t('Banner1.button') }</Button>
            </div>
        </section>
    )
}

export default Banner1
