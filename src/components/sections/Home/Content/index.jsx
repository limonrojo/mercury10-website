import React from 'react';
import { useTranslation } from 'react-i18next'
import Slider from './Slider';
import Button from 'components/common/Button';
import Isologo from 'images/isologo-with-slogan.png';
import Slide0Img from './../assets/Slider/Slides/slide-0.png'
import Slide1Img from './../assets/Slider/Slides/slide-1.png'
import Slide2Img from './../assets/Slider/Slides/slide-2.png'
import Slide3Img from './../assets/Slider/Slides/slide-3.png'
import Slide0Tag from './../assets/Slider/Slides/slide-0-tag.png'
import Slide1Tag from './../assets/Slider/Slides/slide-1-tag.png'
import Slide2Tag from './../assets/Slider/Slides/slide-2-tag.png'
import Slide3Tag from './../assets/Slider/Slides/slide-3-tag.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretRight, faPlus  } from '@fortawesome/free-solid-svg-icons';


const Content = () => {
    const { t } = useTranslation()
    const slides = [
        {
            name:           "M10 Main Board",
            mobileTitle:    () => <span>M10 Main Board</span>,
            image:          Slide0Img,
            width:          578,
            height:         372,
            tagImage:       Slide0Tag,
            button:         {
                buttonText:     "Watch Video",
                buttonIcon:     <FontAwesomeIcon icon={faCaretRight} />,
                buttonAction:   "VIDEO",
                buttonVideo: {
                    channel: 'youtube',
                    videoId: 'LlhmzVL5bm8'
                }
            },
        },
        {
            name:           "TouchScreen",
            mobileTitle:    () => <span>TouchScreen</span>,
            image:          Slide1Img,
            width:          578,
            height:         372,
            tagImage:       Slide1Tag,
            button:         {
                buttonText:     "Watch Video",
                buttonIcon:     <FontAwesomeIcon icon={faCaretRight} />,
                buttonAction:   "VIDEO",
                buttonVideo: {
                    channel: 'youtube',
                    videoId: 'x6cTpJozRd0'
                }
            },
        },
        {
            name:           "GPS LoRa Shield",
            mobileTitle:    () => <span>GPS and LoRa Shield</span>,
            image:          Slide2Img,
            width:          578,
            height:         372,
            tagImage:       Slide2Tag,
            button:         {
                buttonText:     "MORE INFO",
                buttonIcon:     <FontAwesomeIcon icon={faPlus} />,
                buttonAction:   "POPUP",
                buttonActionData: () => (
                    <React.Fragment>
                        <h1>GPS/LoRa shield</h1>
                        <ul>
                            <li>12 to 5 V. power supply for Mercury10 Board.</li>
                            <li>NEO-6M U-Box GPS module.</li>
                            <li>50 ohm external GPS antenna connector.</li>
                            <li>Over distance Serial Port, up to 200 meters at 9600 baud rate.</li>
                            <li>LoRa connecting Port.</li>
                            <li>JoyStick connecting Port.</li>
                            <li>Built-in indicator LEDs.</li>
                            <li>External indication LED connection port.</li>
                        </ul>
                    </React.Fragment>
                )
            },
        },
        {
            
            name:           "Osciloscope & Multimeter",
            mobileTitle:    () => <span>Osciloscope &amp; Multimeter</span>,
            image:          Slide3Img,
            width:          578,
            height:         372,
            tagImage:       Slide3Tag,
            button:         {
                buttonText:     "MORE INFO",
                buttonIcon:     <FontAwesomeIcon icon={faPlus} />,
                buttonAction:   "POPUP",
                buttonActionData: () => (
                    <React.Fragment>
                        <h1>Multimeter &amp; Oscillocope</h1>
                        <h3>Multimeter</h3>
                        <ul>
                            <li>Digital auto selector (Voltimeter, Ohmmeter, Ammeter, Osciloscope).</li>
                            <li>Automatically detects and measures AC/DC voltages.</li>
                            <li>Automatically detects and measures AC/DC current.</li>
                            <li>High current measurement port - rated to 30 Amp.</li>
                            <li>Auto Ranging.</li>
                            <li>Ohmemeter 0 to 10 M Ohms with high precision.</li>
                            <li>Volt meter ratings 0-700 VDC and 0-500 VAC (rms).</li>
                            <li>Ammeter is capable of measuring 0-30A (DC/AC), line voltage can be up to 2500 V peak</li>
                        </ul>
                        <h3>Oscillocope</h3>
                        <ul>
                            <li>Input signal up to 30V peak and maximun frequency of 1Mhz bandwith.</li>
                            <li>Auto Ranging.</li>
                        </ul>
                        <h3>Shield</h3>
                        <ul>
                            <li>Shield input power 12V, regulates power to Mercury10 board.</li>
                        </ul>
                    </React.Fragment>
                )
            },
        },
    ];
    return (
        <div className="Content">
            <div className="cover">
                <div className="text text-1">
                    { t('Home.header.text-1.1') } <b>{ t('Home.header.text-1.2') }</b>
                </div>
                <div className="image isologo"><img src={Isologo} alt="Mercury10" /></div>
                <Slider slides={slides} />
                <div className="container">
                    <div className="text text-2">
                        { t('Home.header.text-2.1') } { t('Home.header.text-2.2') }
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="text text-3 gradientBordersBanner">
                    { t('Home.text-1') }
                </div>
                <Button onClickHandler={() => document.getElementById('Banner2').scrollIntoView()}>{ t('Home.button') }</Button>
            </div>
        </div>
    )
}

export default Content
