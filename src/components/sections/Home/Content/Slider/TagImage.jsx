import React from 'react'
import decorator from './../../assets/Slider/decorator.png'

const TagImage = ({tagImage, alt}) => {
    return (
        <div className="TagImage">
            <div className="mainImage">
                <img src={tagImage} alt={alt} />
            </div>
            <div className="decorator">
                <img src={decorator} alt="Hexagon Decorator" />
            </div>
        </div>
    )
}

export default TagImage
