import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux'
import 'auxiliar'
import Background from './Background';
import Control from './Control';
import Slides from './Slides';
import SliderCounter from 'components/common/SliderCounter';

const Slider = ({ slides, sliderHomeAutoplay }) => {
    const sliderLenght = slides.length;
    const [activeSlide, setActiveSlide] = useState(0);
    useEffect(
        () => {
            if(sliderHomeAutoplay){
                const interval = setInterval(() => { //assign interval to a variable to clear it.
                    setActiveSlide((activeSlide + 1)%sliderLenght)
                }, 5000)
    
                return () => clearInterval(interval); //This is important
            }

        },
        [activeSlide, sliderLenght, sliderHomeAutoplay]
    )
    
    const controlAction = action => {
        switch (action) {
            case "previous":
                setActiveSlide((activeSlide-1).mod(slides.length))    
            break;
            
            case "next":
                setActiveSlide((activeSlide+1).mod(slides.length))
            break;
            
            default:break;
        }
    }
    return (
        <div className="Slider">
            <Background />
            <Slides slides={slides} activeSlide={activeSlide} />
            <Control controlAction={controlAction} />
            <SliderCounter activeSlide={activeSlide} totalSlides={slides.length} />
        </div>
    )
}
const mapStateToProps = (state) => {
    return {
        sliderHomeAutoplay: state.sliderHomeAutoplay
    }
}
export default connect(mapStateToProps, false)(Slider)
