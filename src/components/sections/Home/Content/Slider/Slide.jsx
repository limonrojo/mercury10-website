import React from 'react';
import { connect } from 'react-redux'
import Image from './Image';
import TagImage from './TagImage';
import ButtonWithIcon from 'components/common/ButtonWithIcon';


const Slide = ({width, height, image, tagImage, name, mobileTitle, className, button, loadVideo, loadPopUp }) => {

    const renderButtonAction = (buttonData) => {
        if(buttonData.buttonAction === 'VIDEO'){
            return(
                <React.Fragment>
                    <ButtonWithIcon
                        buttonData={button}
                        onClickHandler={
                            ()=> {
                                loadVideo(button.buttonVideo.videoId)
                            }
                        }
                    />
                </React.Fragment>
            )
        }
        if(buttonData.buttonAction === 'POPUP'){
            return(
                <React.Fragment>
                    <ButtonWithIcon
                        buttonData={button}
                        onClickHandler={
                            ()=> {
                                loadPopUp(button.buttonActionData)
                            }
                        }
                    />
                </React.Fragment>
            )
        }
    }
    return (
        <div className={`Slide ${(className!==undefined)?className:''}`}>
            <Image src={image} alt={name} widht={width} height={height} />
            <TagImage tagImage={tagImage} alt={name} />
            <div className="mobileTitle">{mobileTitle()}</div>
            <div className="buttonZone">
                <div className="diagonal"></div>
                {renderButtonAction(button)}
            </div>
        </div>
    )
}
// const mapStateToProps = (state /*, ownProps*/) => {
//     return {
//         sliderHomeAutoplay: state.sliderHomeAutoplay,
//         videoToLoad: state.videoToLoad,
//     }
// }
const mapDispatchToProps = dispatch => {
    return({
        loadVideo: (videoId) => {
            dispatch({
                type: 'LOAD_VIDEO',
                payload: {
                    videoId: videoId
                }
            })
        },
        loadPopUp: (buttonActionData) => {
            dispatch({
                type: 'LOAD_POPUP',
                payload: {
                    data: buttonActionData
                }
            })
        }
    });
}

export default connect(false, mapDispatchToProps)(Slide)