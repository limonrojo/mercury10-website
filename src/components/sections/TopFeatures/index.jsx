import React from 'react';
import { useTranslation } from 'react-i18next'
import Slider from 'components/common/Slider';
import Title from 'components/common/Title';
import Subtitle from 'components/common/Subtitle';
import './style.scss';
import Screen from './assets/screen.png';
import GPSLora from './assets/gps-lora.png';
import WiFi from './assets/wifi.png';
import SDCard from './assets/sd-card.png';
import RemoteUpload from './assets/ota.png';
import VisualProgramming from './assets/blocky.png';
import VoiceAssistance from './assets/voice-assistance.png';

const TopFeatures = () => {
    const { t } = useTranslation()
    const frontHeader = (
        <header>
            <Title align="right">{ t('TopFeatures.Slider.Front.header.Title') }</Title>
            <Subtitle>{ t('TopFeatures.Slider.Front.header.Subtitle') }</Subtitle>
        </header>
    )
    const topFeatures = [
        {
            image:   Screen,
            title:  t('TopFeatures.Slider.Slides.1.title'),
            description:   t('TopFeatures.Slider.Slides.1.description')
        },
        {
            image:   GPSLora,
            title:  t('TopFeatures.Slider.Slides.2.title'),
            description:   t('TopFeatures.Slider.Slides.2.description')
        },
        {
            image:   WiFi,
            title:  t('TopFeatures.Slider.Slides.3.title'),
            description:   t('TopFeatures.Slider.Slides.3.description')
        },
        {
            image:   SDCard,
            title:  t('TopFeatures.Slider.Slides.4.title'),
            description:   t('TopFeatures.Slider.Slides.4.description')
        },
        {
            image:   RemoteUpload,
            title:  t('TopFeatures.Slider.Slides.5.title'),
            description:   t('TopFeatures.Slider.Slides.5.description')
        },
        {
            image:   VisualProgramming,
            title:  t('TopFeatures.Slider.Slides.6.title'),
            description:   t('TopFeatures.Slider.Slides.6.description')
        },
        {
            image:   VoiceAssistance,
            title:  t('TopFeatures.Slider.Slides.7.title'),
            description:   t('TopFeatures.Slider.Slides.7.description')
        }
    ];
    return (
        <section id="TopFeatures">
            <Slider slides={topFeatures} frontHeader={frontHeader} />
        </section>
    )
}

export default TopFeatures
