import React from 'react';
// import { useTranslation } from 'react-i18next';
import Image from 'components/common/Image'
import IDE from './assets/ide.png'
import Codes from './assets/codes.png'
import Button from 'components/common/Button';
import './style.scss';

const Banner3 = () => {
    // const { t } = useTranslation()
    return (
        <section id="Banner3" className="Banner">
            <div className="container">
                <div className="columns">
                    <div className="column">
                        <Image src={IDE} />
                        <p>Download official Mercury10 IDE<br/>for coding your dreams.</p>
                        <Button onClickHandler={()=>window.location='https://mercury10.com/mws/dashboard.php'}>Download IDE</Button>
                        {/* <div className="text">{t('Banner3.text-1')}</div>
                        <div className="textSmall">{t('Banner3.text-2')}</div> */}
                    </div>
                    <div className="column">
                        <Image src={Codes} />
                        <p>Download existing code examples and libraries<br/>for your IoT projects</p>
                        <Button onClickHandler={()=>window.location='https://mercury10.com/mws/dashboard.php'}>Go to Downloads Page</Button>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default Banner3
