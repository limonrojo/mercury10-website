import React from 'react';
import { connect } from 'react-redux'
import { useTranslation } from 'react-i18next'
import Isologo from 'images/isologo-with-slogan.svg';
import './style.scss';
import SocialLinks from 'components/common/SocialLinks';
import SectionsLinks from 'components/common/SectionsLinks';
import Image from 'components/common/Image';

const Footer = ({routes}) => {
    const { t } = useTranslation()
    return (
        <footer id="Footer">
            <div className="container">
                <div className="columns">
                    <div className="column">
                        <div className="logo">
                            <Image src={Isologo} alt="Mercury10" />
                        </div>
                        <SocialLinks />
                    </div>
                    <div className="column column-SectionLinks">
                        <SectionsLinks direction="column" />
                    </div>
                </div>
            </div>
            <div className="disclaimer">{ t('Footer.disclaimer') }</div>
            <div className="legal">
                <div>{ t('Footer.legal.arr') }</div>
                <br/>
                <div>
                    <ul>
                        <li><a target="_blank" rel="noreferrer" href={routes.disclaimer}>Disclaimer</a></li>
                        <li><a target="_blank" rel="noreferrer" href={routes.cookiePolicy}>Cookie Policy</a></li>
                        <li><a target="_blank" rel="noreferrer" href={routes.returnPolicy}>Return Policy</a></li>
                        <li><a target="_blank" rel="noreferrer" href={routes.privacyPolicy}>Privacy notice</a></li>
                    </ul>
                </div>
            </div>
        </footer>
    )
}
const mapStateToProps = state =>{
    return {
        routes: state.routes
    }
}
export default connect(mapStateToProps)(Footer)
