/* eslint-disable no-extend-native */
Number.prototype.mod = function(n) {
    return ((this%n)+n)%n;
}

Number.prototype.pad = function(size) {
    var sign = Math.sign(this) === -1 ? '-' : '';
    return sign + new Array(size).concat([Math.abs(this)]).join('0').slice(-size);
  }