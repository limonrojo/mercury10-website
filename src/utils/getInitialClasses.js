export const getSlidesInitialClasses = (activeSlide, slidesLenght) => {
    const initialClasses = {
        [(activeSlide-2).mod(slidesLenght)]:"leftDisplace",
        [(activeSlide-1).mod(slidesLenght)]:"leftInactive",
        [activeSlide]: "active",
        [(activeSlide+1).mod(slidesLenght)]:"rightInactive",
        [(activeSlide+2).mod(slidesLenght)]:"rightDisplace"
    }
    return initialClasses
}
export default getSlidesInitialClasses