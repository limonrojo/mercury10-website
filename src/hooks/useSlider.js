import { useState, useEffect } from 'react';

export const useSlider = (data, time=5000, active=true) => {
    const sliderLenght = data.length;
    const [activeSlide, setActiveSlide] = useState(0);
    
    useEffect(
        () => {
            if(active){
                const interval = setInterval(() => { //assign interval to a variable to clear it.
                    setActiveSlide((activeSlide + 1)%sliderLenght)
                }, time)
    
                return () => clearInterval(interval); //This is important
            }

        },
        [active, activeSlide, sliderLenght, time]
    )
    const controlAction = action => {
        switch (action) {
            case 'previous':
                setActiveSlide((activeSlide-1).mod(data.length))    
            break;
            
            case 'next':
                setActiveSlide((activeSlide+1).mod(data.length))
            break;
            
            default:break;
        }
    }

    return [activeSlide, controlAction]
}

export default useSlider
